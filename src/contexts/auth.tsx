import { createContext, ReactNode, useEffect, useState } from 'react';
import { api } from '../services/api';

type User = {
    id: string;
    name: string;
    login: string;
    avatar_url: string;
}

type AuthContextData = {
    user: User | null;
    signInUrl: string;
    signOut: () => void;
}

export const AuthContext = createContext({} as AuthContextData)

type AuthProvider = {
    children: ReactNode;
}

type AuthResponse = {
    token: string;
    user: {
        id: string;
        avatar_url: string;
        name: string;
        login: string;
    }
}

export function AuthProvider(props: AuthProvider) {
    const [user, setUser] = useState<User | null>(null)

    const signInUrl = `https://github.com/login/oauth/authorizire?scope=user&client_id=eg65edda599622ddfa772&redirect_uri=http://localhost:3000`;
    
    async function signIn(githubCode: string) {
        const response = await api.post<AuthResponse>('authenticate', {
            code: githubCode,
        })

        const { token, user } = response.data

        localStorage.setItem('@dowhile:token', token)

        api.defaults.headers.common.authorization = `Bearer ${token}`;

        setUser(user)
    }

    function signOut () {
        setUser(null)
        localStorage.removeItem('@dowhile:token')
    }
    
    useEffect(() => {
        const token = localStorage.getItem('@dowhile:token')

        if (token) {
            api.defaults.headers.common.authorization = `Bearer ${token}`;

            api.get<User>('profile').then(response => {
                console.log(response.data);
                setUser(response.data)
            })
        }
    }, [])

    useEffect(() => {
        const url = window.location.href; // busca a url da aplicação
        const hasGithubCode = url.includes('?code='); // verifica se no texto da url inclue o code=

        if (hasGithubCode) {
            // pegando o codigo da url com githubCode
            const [urlWithoutCode, githubCode] = url.split('?code=')
            console.log({ urlWithoutCode, githubCode })

            window.history.pushState({}, '', urlWithoutCode) // limpa o codigo da url

            signIn(githubCode)
        }
    }, [])

    return (
        <AuthContext.Provider value={{ signInUrl, user, signOut }}>
            {props.children}
        </AuthContext.Provider>
    )
}