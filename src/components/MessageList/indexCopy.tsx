import styles from './styles.module.scss';

import maidenLogo from '../../assets/ironmaiden.svg';

export function MessageList() {

    return (
        <div className={styles.messageListWrapper}>
            <img src={maidenLogo} alt="Eddie" />

            <ul className={styles.messageList}>
                <li className={styles.message}>
                    <p className={styles.messageContent}>Iron Maiden é a melhor banda de Heavy Metal do mundo. UP THE IRONS!!!</p>
                    <div className={styles.messageUser}>
                        <div className={styles.userImage}>
                            <img src="https://github.com/alekyscoelho.png" alt="Alekys Coelho" />
                        </div>
                        <span>Alekys Coelho</span>

                    </div>
                </li>

                <li className={styles.message}>
                    <p className={styles.messageContent}>Iron Maiden é a melhor banda de Heavy Metal do mundo. UP THE IRONS!!!</p>
                    <div className={styles.messageUser}>
                        <div className={styles.userImage}>
                            <img src="https://github.com/alekyscoelho.png" alt="Alekys Coelho" />
                        </div>
                        <span>Alekys Coelho</span>

                    </div>
                </li>

                <li className={styles.message}>
                    <p className={styles.messageContent}>Iron Maiden é a melhor banda de Heavy Metal do mundo. UP THE IRONS!!!</p>
                    <div className={styles.messageUser}>
                        <div className={styles.userImage}>
                            <img src="https://github.com/alekyscoelho.png" alt="Alekys Coelho" />
                        </div>
                        <span>Alekys Coelho</span>

                    </div>
                </li>
            </ul>

        </div>
    )
}