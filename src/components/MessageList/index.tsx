import { useEffect, useState } from 'react';
import io from 'socket.io-client'; // ouvir coisas em tempo real do backend
import { api } from '../../services/api';


import styles from './styles.module.scss';

import maidenLogo from '../../assets/ironmaiden.svg';

type Message = {
    id: string;
    text: string;
    user: {
        name: string;
        avatar_url: string;
    }
}

const messagesQueue: Message[] = [];

const socket = io('http://localhost:8000');

socket.on('new_message', (newMessage: Message) => {
    console.log(newMessage);
    messagesQueue.push(newMessage);
})

export function MessageList() {
    const [messages, setMessages] = useState<Message[]>([])

    //verifica se na fila tem pelo menos 1 mensagem. Se tiver pelo menos 1 mensagem
    // sobrepoem(setMessagens) o valor do array de mensagens com um novo array com novas informações.
    useEffect(() => {
        setInterval(() => {
            if (messagesQueue.length > 0) {
                setMessages(prevState => [
                    messagesQueue[0], // mensagem mais antiga na fila
                    prevState[0], // messagem que já exista no array de mensagens
                    prevState[1], // messagem existente na posição 1
                ].filter(Boolean)) // filtra o array, remove valores do tipo false(undefined, null, ...)

                messagesQueue.shift() // remove o primeiro item, ou seja o mais antigo, da lista
            }
        }, 3000)
    }, [])

    useEffect(() => {
        api.get<Message[]>('messages').then(response => {
            setMessages(response.data);
        })
    }, [])
    return (
        <div className={styles.messageListWrapper}>
            <img src={maidenLogo} alt="Eddie" />

            <ul className={styles.messageList}>
                {messages.map(message => {
                    return (
                        <li key={message.id} className={styles.message}>
                            <p className={styles.messageContent}>{message.text}</p>
                            <div className={styles.messageUser}>
                                <div className={styles.userImage}>
                                    <img src={message.user.avatar_url} alt={message.user.name} />
                                </div>
                                <span>{message.user.name}</span>
                            </div>
                        </li>
                    )
                })}

            </ul>

        </div>
    )
}